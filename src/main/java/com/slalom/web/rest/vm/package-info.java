/**
 * View Models used by Spring MVC REST controllers.
 */
package com.slalom.web.rest.vm;
