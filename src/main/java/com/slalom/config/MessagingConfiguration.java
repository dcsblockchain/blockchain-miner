package com.slalom.config;

import com.slalom.messaging.BlockConsumerChannel;
import com.slalom.messaging.BlockProducerChannel;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.messaging.Source;

/**
 * Configures Spring Cloud Stream support.
 *
 * This works out-of-the-box if you use the Docker Compose configuration at "src/main/docker/kafka.yml".
 *
 * See http://docs.spring.io/spring-cloud-stream/docs/current/reference/htmlsingle/
 * for the official Spring Cloud Stream documentation.
 */
@EnableBinding(value = { Source.class, BlockConsumerChannel.class, BlockProducerChannel.class})
public class MessagingConfiguration {
}
