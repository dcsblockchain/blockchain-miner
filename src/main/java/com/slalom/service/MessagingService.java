package com.slalom.service;

import com.slalom.blockchain.Block;
import com.slalom.messaging.BlockConsumerChannel;
import com.slalom.messaging.BlockProducerChannel;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.MessageChannel;
import org.springframework.stereotype.Service;

@Service
public class MessagingService {

    private final MiningService miningService;
    private MessageChannel blockMinedChannel;

    public MessagingService(MiningService miningService, BlockProducerChannel blockProducerChannel) {
        this.miningService = miningService;
        this.blockMinedChannel = blockProducerChannel.blockMined();
    }

    // TODO stop mining when another miner finds the nonce first
    @StreamListener(BlockConsumerChannel.CHANNEL)
    public void blockReceived(Block block) {
        Block minedBlock = miningService.mineBlock(block);
        blockMinedChannel.send(MessageBuilder.withPayload(minedBlock).build());
    }
}
