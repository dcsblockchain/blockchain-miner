package com.slalom.service;

import com.slalom.blockchain.Block;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.stereotype.Service;

import static org.apache.commons.codec.digest.MessageDigestAlgorithms.SHA_256;

@Service
public class MiningService {

    public Block mineBlock(Block block) {
        DigestUtils md = new DigestUtils(SHA_256);
        for (int i = 0; i < Integer.MAX_VALUE; i++) {
            String currentHash = md.digestAsHex(
            block.getPreviousHash() +
                Long.toString(block.getIndex()) +
                    block.getData().toString() +
                    Long.toString(block.getTimeStamp()) + Integer.toString(i));
            if (currentHash.startsWith("00")) { // the level of difficulty, the more leading zeros, the harder it will be to find the nonce
                block.setNonce(Integer.toString(i));
                block.setHash(currentHash);
                break;
            }
        }

        return block;
    }
}
